﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xceed.Words.NET;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            string fileName = @"E:\TT18Word.doc";
            var doc = DocX.Create(fileName);

            Formatting table_header = new Formatting();
            table_header.Bold = true;
            table_header.FontFamily = new Font("Times New Roman");
            table_header.Size = 13;
            table_header.UnderlineColor = System.Drawing.Color.Gray;

            Formatting table_row = new Formatting();
            table_row.FontFamily = new Font("Times New Roman");
            table_row.Size = 14;

            Table t = doc.AddTable(2, 2);
            t.SetWidths(new float[] { 190, 290 });
            t.Design = TableDesign.None;
            t.Alignment = Alignment.center;
            t.AutoFit = AutoFit.ColumnWidth;
            t.Rows[0].Cells[0].Paragraphs.First().InsertText("BỘ GIÁO DỤC VÀ ĐÀO TẠO", false, table_header);
            t.Rows[0].Cells[0].Paragraphs.First().Alignment = Alignment.center;
            t.Rows[0].Cells[1].Paragraphs.First().InsertText("CỘNG HOÀ XÃ HỘI CHỦ NGHĨA VIỆT NAM \n Độc lập - Tự do -Hạnh phúc", false, table_header);
            t.Rows[0].Cells[1].Paragraphs.First().Alignment = Alignment.center;
            t.Rows[1].Cells[0].Paragraphs.First().InsertText("\nSố:  17/2018/TT-BGDĐT", false, table_row);
            t.Rows[1].Cells[0].Paragraphs.First().Alignment = Alignment.center;
            t.Rows[1].Cells[1].Paragraphs.First().InsertText("\nHà Nội, ngày 22 tháng 8 năm 2018", false, table_row);
            t.Rows[1].Cells[1].Paragraphs.First().Alignment = Alignment.center;
            doc.InsertTable(t);

            string title = Environment.NewLine + "THÔNG TƯ" + Environment.NewLine + "Ban hành Quy định về kiểm định chất lượng giáo dục và công nhận đạt chuẩn quốc gia đối với trường tiểu học";
            //text  
            string textParagraph = Environment.NewLine + "\tCăn cứ Luật giáo dục ngày 14 tháng 6 năm 2005; Luật sửa đổi, bổ sung một số điều của Luật giáo dục ngày 25 tháng 11 năm 2009;" + Environment.NewLine
                + "\tCăn cứ Nghị định số 69/2017/NĐ-CP ngày 25 tháng 5 năm 2017 của Chính phủ quy định chức năng, nhiệm vụ, quyền hạn và cơ cấu tổ chức của Bộ Giáo dục và Đào tạo;" + Environment.NewLine
                + "\tCăn cứ Nghị định số 75/2006/NĐ-CP ngày 02 tháng 8 năm 2006 của Chính phủ quy định chi tiết và hướng dẫn thi hành một số điều của Luật giáo dục; Nghị định số 31/2011/NĐ-CP ngày 11 tháng 5 năm 2011 của Chính phủ sửa đổi, bổ sung một số điều của Nghị định số 75/2006/NĐ-CP ngày 02 tháng 8 năm 2006 của Chính phủ quy định chi tiết và hướng dẫn thi hành một số điều của Luật giáo dục; Nghị định số 07/2013/NĐ-CP ngày 09 tháng 01 năm 2013 của Chính phủ sửa đổi điểm b khoản 13 Điều 1 của Nghị định 31/2011/NĐ-CP ngày 11 tháng 5 năm 2011 sửa đổi, bổ sung một số điều của Nghị định 75/2006/NĐ-CP ngày 02 tháng 8 năm 2006 của Chính phủ quy định chi tiết và hướng dẫn thi hành một số điều của Luật giáo dục;" + Environment.NewLine
                + "\tTheo đề nghị của Cục trưởng Cục Quản lý chất lượng và Vụ trưởng Vụ Giáo dục Tiểu học," + Environment.NewLine
                + "\tBộ trưởng Bộ Giáo dục và Đào tạo ban hành Thông tư ban hành Quy định về kiểm định chất lượng giáo dục và công nhận đạt chuẩn quốc gia đối với trường tiểu học." + Environment.NewLine
                + "\tĐiều 1. Ban hành kèm theo Thông tư này Quy định về kiểm định chất lượng giáo dục và công nhận đạt chuẩn quốc gia đối với trường tiểu học." + Environment.NewLine
                + "\tĐiều 2. Thông tư này có hiệu lực thi hành kể từ ngày 10 tháng 10 năm 2018." + Environment.NewLine
                + "\tThông tư này thay thế các quy định về tiêu chuẩn đánh giá chất lượng giáo dục và quy trình, chu kỳ kiểm định chất lượng giáo dục đối với trường tiểu học tại Thông tư số 42/2012\tT-BGDĐT ngày 23 tháng 11 năm 2012 của Bộ trưởng Bộ Giáo dục và Đào tạo ban hành Quy định về tiêu chuẩn đánh giá chất lượng giáo dục và quy trình, chu kỳ kiểm định chất lượng giáo dục cơ sở giáo dục phổ thông, cơ sở giáo dục thường xuyên và thay thế Thông tư số 59/2012\tT-BGDĐT ngày 28 tháng 12 năm 2012 của Bộ trưởng Bộ Giáo dục và Đào tạo ban hành Quy định về tiêu chuẩn đánh giá, công nhận trường tiểu học đạt mức chất lượng tối thiểu, trường tiểu học đạt chuẩn quốc gia." + Environment.NewLine
                + "\tĐiều 3. Chánh Văn phòng, Cục trưởng Cục Quản lý chất lượng, Vụ trưởng Vụ Giáo dục Tiểu học, Thủ trưởng các đơn vị có liên quan thuộc Bộ Giáo dục và Đào tạo, Chủ tịch Uỷ ban nhân dân các tỉnh, thành phố trực thuộc Trung ương, Giám đốc sở giáo dục và đào tạo chịu trách nhiệm thi hành Thông tư này./." + Environment.NewLine;


            Formatting titleFormat = new Formatting();
            titleFormat.FontFamily = new Font("Times New Roman");
            titleFormat.Size = 14;
            titleFormat.FontColor = System.Drawing.Color.Black;
            titleFormat.Bold = true;

            //Formatting Text Body  
            Formatting textParagraphFormat = new Formatting();
            textParagraphFormat.FontFamily = new Font("Times New Roman");
            textParagraphFormat.Size = 14;

            //Insert title  
            Paragraph paragraphTitle = doc.InsertParagraph(title, false, titleFormat);
            paragraphTitle.Alignment = Alignment.center;
            //Insert body  
            doc.InsertParagraph(textParagraph, false, textParagraphFormat);

            doc.Save();

            Process.Start("WINWORD.EXE", fileName);
        }
    }
}
